
CXX=g++
CPPFLAGS=-Wall
CXXFLAGS=-g -std=c++0x
LDLIBS=
OBJDIR=obj
vpath %.cpp src src/huffman
vpath %.h src src/huffman

targets = huff expand
depends = $(addprefix $(OBJDIR)/, huffman.o)

all: $(targets)

huff : obj/huff.o $(depends)
	$(CXX) $(LDFLAGS) $(CPPFLAGS) $(CXXFLAGS) $^ -o $@

expand : obj/expand.o $(depends)
	$(CXX) $(LDFLAGS) $(CPPFLAGS) $(CXXFLAGS) $^ -o $@

obj/huffman.o : huffman.cpp
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@

obj/expand.o : expand.cpp 
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@

obj/huff.o : huff.cpp 
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@

.PHONY : clean
clean:
	rm -rf $(objects) $(targets) *.o
