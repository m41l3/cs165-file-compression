Paul Daehan Kang
Christopher Gutierrez 25654470

For the Lempel Ziv implementation, the dictionary was implemented as a hash table. The dictionary entries are added whenever 
we attempt to search for a sub string of the look ahead buffer, but it is not in the dicitonary. Every time the window shifts,
the entries are updated to change their offset.




Benchmarks

filename		 news
size         377.109KB
----		 
HUFFsiz		   438.532KB
%save		     43.00% 
t-cmprss		 0.68s
t-expand		 time to decompress from HUFF
----
LZ1siz		 341.141KB 
%save		 9.53%
t-cmprss		 24.20s
t-expand		 time to decompress from LZ variation 1
----	
LZ2siz		341.395KB 
%save		9%
t-cmprss		 25.61s
t-expand		 time to decompress from LZ variation 2
----
???siz		 compressed file size using utility program (specify)
%save		 percentage compression savings for utility program
t-cmprss		 time to compress using utility program
t-expand		 time to decompress from utility program

filename		 book1
size         768.771KB 
----		 
HUFFsiz		   246.544KB
%save		     43.00% 
t-cmprss		 0.68s
t-expand		 time to decompress from HUFF
----
LZ1siz		 668.182KB 
%save		 13.08%
t-cmprss		 24.20s
t-expand		 time to decompress from LZ variation 1
----	
LZ2siz		603.672KB
%save		21.47%
t-cmprss		 47.40s
t-expand		 time to decompress from LZ variation 2
----
???siz		 compressed file size using utility program (specify)
%save		 percentage compression savings for utility program
t-cmprss		 time to compress using utility program
t-expand		 time to decompress from utility program

filename		 kennedy.xls
size         1029.744KB
----		 
HUFFsiz		   462.859kb
%save		     43.00% 
t-cmprss		 0.68s
t-expand		 time to decompress from HUFF
----
LZ1siz		 415.153KB
%save		 59.68%
t-cmprss		 19.96s
t-expand		 time to decompress from LZ variation 1
----	
LZ2siz		393.344KB
%save		61.80%
t-cmprss		 38.15s
t-expand		 time to decompress from LZ variation 2
----
???siz		 compressed file size using utility program (specify)
%save		 percentage compression savings for utility program
t-cmprss		 time to compress using utility program
t-expand		 time to decompress from utility program