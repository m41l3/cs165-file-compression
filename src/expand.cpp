#include <fstream>
#include <iostream>
#include <string>
#include "huffman/huffman.h"

#define HUFF 13
#define LZ1 17
#define LZ2 19

using namespace std;

int main(int argc, char* argv[]) {
  string usage = "Usage: expand <file>\n";
  if (argc != 2) {
    cout << usage;
    return 0;
  }
  string fstr(argv[1]);
  int lastindex = fstr.find_last_of(".");
  ifstream ifs(fstr);
  ofstream ofs(fstr.substr(0, lastindex).append(".out"), ofstream::out);

  clock_t t0 = clock(); // Execution timing

  char magic; 
  if (ifs.good()) {
    magic = ifs.get();
    switch(magic) {
      case HUFF: 
        {
          HuffmanTree hf;
          hf.build_from_enc(ifs);
          hf.print_tree();
          hf.decode(ifs, ofs);
          break;
        }
      case LZ1:
        {
          //TODO
          cout << "Not implemented\n";
          break;
        }
      case LZ2:
        {
          //TODO
          cout << "Not implemented\n";
          break;
        }
      default:

        cout << "Could not determin compression type.\n" << usage;
    }
  }
  ifs.close();
  ofs.close();

  cout << "Compression took: " << ((float)(clock()-t0))/CLOCKS_PER_SEC << endl;

  return 0;
      
}
