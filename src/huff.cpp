#include <iostream>
#include <fstream>
#include "huffman/huffman.h"

using namespace std;

int main(int argc, char* argv[]) {
  if (argc != 2) { 
    cout << "Usage: huff <file>\n";
    return 0;
  }
  clock_t t0=clock(); // Execution timing

  string fstr(argv[1]);
  // get the file input
  ifstream ifs(fstr);

  // designate the compressed file output
  int lastindex = fstr.find_last_of(".");
  ofstream ofs(fstr.substr(0, lastindex).append(".huf"));
  HuffmanTree hf;
  hf.build(ifs);
  hf.print_tree();
  ifs.seekg(ios::beg);
  ifs.clear();
  hf.encode(ifs, ofs);

  ifs.close();
  ofs.close();

  cout << "Compression took: " << ((float)(clock()-t0))/CLOCKS_PER_SEC << endl;

  return 0;
}
