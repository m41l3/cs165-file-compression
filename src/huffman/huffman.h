#ifndef HUFFMAN_H
#define HUFFMAN_H
#include <iostream>
#include <unordered_map>
#include <vector>
#include <limits.h>

#define MAGIC_NUMBER 13
#define INTERN_NODE_TYPE 0
#define LEAF_NODE_TYPE 1
#define BYTE_SIZE sizeof(char)*CHAR_BIT
#define STREAM_SIZE 8192
typedef unsigned int uint_16;

struct bostream {
  std::ostream& out;
  char buf;
  int pos;
  int cc;
  bostream(std::ostream& o) 
    : out(o), buf(0), pos(BYTE_SIZE-1), cc(0) {};
  void put(bool b) {
    if (cc == STREAM_SIZE-128) {
      std::cout << "*** FLUSH STREAM***\n";
      out.flush(); // monitor steam limit
      cc = 0;
    } 
    if (pos == -1) {
      pos = BYTE_SIZE-1;
      out << buf;
      buf = 0;
      cc++;
    }
    buf = buf | (b << pos);
    pos--;
  }
  void close() {
    if (pos != -1) {
      out << buf;
    }
    out.flush();
  };
};

struct bistream {
  std::istream& in;
  char buf;
  int pos;
  bistream(std::istream& is) 
    : in(is), buf(0), pos(-1) {};
  bool get() {
    if (pos == -1) {
      pos = BYTE_SIZE-1;
      buf = in.get();
    }
    return ((buf >> pos--)&1);
  }
  bool good() {
    return in.good() || pos != -1;
  }
  void close() {
    // no-op
  };
};

 

class HuffmanTree {
  private:

  // ====== Structures =======

  struct node {
    int type;
    int value; // value used only when building tree from frequency
    node* parent; 
  };

  struct intern_node : node {
    intern_node() : left(0), right(0) {type = 0; value = 0;}
    node *left, *right;
  };

  struct leaf_node : node {
    leaf_node(char _c) : c(_c) {type = 1; value = 1;}
    char c;
  };

  // Used in 'build_from_file' for sorting frequencies
  struct lnptr_comparator {
    bool operator() (HuffmanTree::leaf_node* ln0, 
        HuffmanTree::leaf_node* ln1) {
      return (ln0->value > ln1->value); 
    }
  } lnc;

  // ====== Instance variables =======
  std::vector<intern_node*> intern_nodes;
  std::vector<leaf_node*> leaf_nodes;
  std::unordered_map<char, leaf_node*> char_map; // for quick access to characters
  node* head;


  // ====== Private functions ======
  
  // Initialize the char_map to store the character frequencies.
  // The character is the key and the leaf_node is the value; the 
  // map values are sorted.
  void generate_leaf_nodes(std::istream&); 
  void generate_intern_nodes(); 

  void pack_tree(std::ostream&);
  void unpack_tree(std::istream&);

  public:
  HuffmanTree();
  /* HuffmanTree(const HuffmanTree&); */
  ~HuffmanTree();

  void build(std::istream&);
  void build_from_file(std::istream&);
  void build_from_enc(std::istream&);
  void encode(std::istream&, std::ostream&);
  void decode(std::istream&, std::ostream&);

  size_t size();
  node* get_head();
  void print_tree();
  
};

#endif //HUFFMAN_H
