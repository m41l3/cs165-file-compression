#include "huffman.h"
#include <iostream>
#include <unordered_map>
#include <vector>
#include <list>
#include <algorithm>

#include <fstream> // main();


#define VERBOSE 0 

HuffmanTree::HuffmanTree() {}

HuffmanTree::~HuffmanTree() { 
  for (std::vector<intern_node*>::iterator it = intern_nodes.begin();
      it != intern_nodes.end(); it++) {
    delete(*it);
  }
  for (std::vector<leaf_node*>::iterator it = leaf_nodes.begin();
      it != leaf_nodes.end(); it++) {
    delete(*it);
  }
}

/* ====== Public member functions ======== */

void HuffmanTree::build(std::istream& in) {
  if (!in.good()) {
    if (VERBOSE) std::cout << "* ERROR: bad stream\n"; 
    return;
  }
  int magic = in.get();
    if (VERBOSE) std::cout << "* Magic? " << magic << std::endl; 
  if (magic == 13) {
    build_from_enc(in);
  } else {
    in.seekg(std::ios::beg);
    in.clear();
    build_from_file(in);
  }
}

void HuffmanTree::build_from_file(std::istream& in) {
  if (VERBOSE) std::cout << "Building tree from file...\n"; 
  if (!in.good()) {
    if (VERBOSE) std::cout << "* ERROR: bad stream\n"; 
    return;
  }
  generate_leaf_nodes(in);
  generate_intern_nodes();
  if (VERBOSE) std::cout << "* tree size: " << size() << std::endl;
}

void HuffmanTree::build_from_enc(std::istream& in) {
  if (!in.good()) {
    if (VERBOSE) std::cout << "* ERROR: bad stream\n"; 
    return;
  }
  if (VERBOSE) std::cout << "Building tree from encoding..\n";
  unpack_tree(in);
  if (VERBOSE) std::cout << "* encoded tree size: " << size() << std::endl;
}

void HuffmanTree::encode(std::istream& in, std::ostream& out) {
  if (!in.good()) {
    if (VERBOSE) std::cout << "* ERROR: bad input stream\n"; 
    return;
  } else if (!out.good()) {
    if (VERBOSE) std::cout << "* ERROR: bad output stream\n"; 
    return;
  }
  if (VERBOSE) std::cout << "* encoding magic number...\n";
  out.put(MAGIC_NUMBER);

  if (VERBOSE) std::cout << "* encoding tree...\n";
  pack_tree(out);

  in.seekg(std::ios::beg);
  in.clear();
  if (VERBOSE) std::cout << "* encoding file stream...\n";
  char c;
  node* n;
  bostream bos(out);
  std::list<bool> bstack;
  while(in.good()) {
    c = in.get();
    /* if (VERBOSE) std::cout << c << ":"; */ 
    n = char_map[c];
    while (n->parent != NULL) {
      bstack.push_back(n == ((intern_node*)n->parent)->right);
      n = n->parent;
    }
    for (auto it = bstack.rbegin(); it != bstack.rend(); it++) {
      bos.put(*it);
      /* if (VERBOSE) std::cout << *it; */
    }
    /* if (VERBOSE) std::cout << std::endl; */
    bstack.clear();
  }
  bos.close();
}

void HuffmanTree::decode(std::istream& in, std::ostream& out) {
  if (!in.good()) {
    if (VERBOSE) std::cout << "* ERROR: bad input stream\n"; 
    return;
  } else if (!out.good()) {
    if (VERBOSE) std::cout << "* ERROR: bad output stream\n"; 
    return;
  }
  if (VERBOSE) std::cout << "Decoding compressed stream...\n";
  bool b;
  node* n;
  bistream bis(in);
  while(1) {
    n = head;
    while(n->type == INTERN_NODE_TYPE && bis.good()) { // Shouldn't have a problem if encoded stream is formatted correctly
      b = bis.get();
      if (VERBOSE) std::cout << b;
      if (b)
        n = ((intern_node*)n)->right;
      else
        n = ((intern_node*)n)->left;
    }
    if (VERBOSE) std::cout << "->" << ((leaf_node*)n)->c << std::endl;

    if(((leaf_node*)n)->c == -1) 
      break;
    else
      out << ((leaf_node*)n)->c;
  }
  /* bis.close(); */
}

/* ====== Private member functions ======== */

void HuffmanTree::generate_leaf_nodes(std::istream& in) {
  if (VERBOSE) std::cout << "Generating leaf nodes..\n";
  // Add the characters and count their frequencies
  while (in.good()) {
    char c = (char) in.get();
    if (char_map.count(c)) {
      char_map[c]->value++;
    } else {
      leaf_node* lnptr = new leaf_node(c);
      leaf_nodes.push_back(lnptr);
      std::pair<char, leaf_node*> p(c, lnptr);
      char_map.insert(p);
    }
  }

  // Sort the leaf nodes by their frequencies
  std::sort(leaf_nodes.rbegin(), leaf_nodes.rend(), lnc);
}

void HuffmanTree::generate_intern_nodes() {
  if (VERBOSE) std::cout << "Generating internal nodes..\n";
  std::vector<node*> merge_options;
  auto lnit = leaf_nodes.begin(); 

  // Initially fill the merge_options
  if (leaf_nodes.size() < 2) {
    // TODO
    return;
  } else {
    merge_options.insert(merge_options.begin(), lnit, lnit+2);
    lnit = lnit + 2;
  }

  // TODO: The order of the internal nodes will not matter, but
  // that of the leaf nodes will later on in the compression process
  
  // Merge the smallest values
  while (merge_options.size() != 1) {
    auto mit = merge_options.begin();
    intern_node* nn = new intern_node();
    nn->value = (*mit)->value + (*(mit+1))->value;
    nn->left = (*mit);
    nn->right = (*(mit+1));
    (*mit)->parent = (*(mit+1))->parent = nn;
    mit = merge_options.erase(mit, mit+2);

    // place the new internal node back into the merge options
    for (mit = merge_options.begin(); ; mit++) {
      if (mit == merge_options.end() || nn->value <= (*mit)->value) {
        merge_options.insert(mit, nn);
        break;
      }
    }
    intern_nodes.insert(intern_nodes.end(), nn);

    // Refill the merge_options with up to two values <= the smallest option
    int insert_count = 0;
    int value = merge_options.front()->value;
    mit = merge_options.begin();
    while(lnit != leaf_nodes.end() && (*lnit)->value <= value && insert_count < 2) {
        merge_options.insert(mit, (*lnit++));
        insert_count++;
    } 
    // If the end of the leaf node list has not been reached and
    // no values have been inserted, add a value so that there at least 2 options
    if (insert_count == 0 && merge_options.size() == 1) {
      for (mit = merge_options.begin(); lnit != leaf_nodes.end(); mit++) {
        if (mit == merge_options.end() || (*lnit)->value <= value) {
          merge_options.insert(merge_options.end(), (*lnit++));
          break;
        }
      }
    }
  }
  head = merge_options[0];
  head->parent = NULL;
}

//==================
//
void uint_to_bytes(uint_16 i, std::ostream& out) {
  out << (char)(i >> BYTE_SIZE*3);
  out << (char)(i >> BYTE_SIZE*2);
  out << (char)(i >> BYTE_SIZE*1);
  out << (char)(i >> 0);
}

uint_16 bytes_to_uint(std::istream& in) {
  uint_16 i = 0;
  i = i | (in.get() << BYTE_SIZE*3);
  i = i | (in.get() << BYTE_SIZE*2);
  i = i | (in.get() << BYTE_SIZE*1);
  i = i | (in.get() << 0);
  return i;
}

//==================
void HuffmanTree::pack_tree(std::ostream& out) {
  if (VERBOSE) std::cout << "Packing tree...\n";
  if (size() == 0) {
    out << 0;
    return;
  }

  // output the size of the tree
  uint_to_bytes(size(), out);

  // In-order traversal of the tree  
  char buf; 
  node* n;
  int bit_pos = BYTE_SIZE-1;
  std::list<node*> toVisit = {head};
  std::list<char> charList;
  bostream bos(out);
  while (!toVisit.empty()) {
    n = toVisit.front();
    toVisit.pop_front();

    // Visit - insert the bit corresponding to the current node
    if (n->type == LEAF_NODE_TYPE) {
      bos.put(1);
      charList.push_back(((leaf_node*)n)->c);
    } else if (n->type == INTERN_NODE_TYPE) {
      bos.put(0);
      // push children into the toVisit list
      toVisit.push_front(((intern_node*)n)->right);
      toVisit.push_front(((intern_node*)n)->left);
    }
  }
  bos.close();

  // Write the leaf characters to the stream
  for (auto it = charList.begin(); it != charList.end(); it++) {
    out << *it;
  }
}

void HuffmanTree::unpack_tree(std::istream& in) {
  if (VERBOSE) std::cout << "Unpacking tree...\n";

  uint_16 tree_size = bytes_to_uint(in);
  std::list<intern_node*> toVisit;

  bistream bis(in);
  bool bit; 
  bool last_bit;
  uint_16 node_count = 0;
  intern_node* inode;
  leaf_node* lnode;
  while(node_count < tree_size) {
    bit = bis.get();
    if (VERBOSE) std::cout << bit;
    if (bit == INTERN_NODE_TYPE) {
      inode = new intern_node();
      inode->value = 0; // mark as not visited
      intern_nodes.push_back(inode);
      toVisit.push_front(inode);
      if (toVisit.size() == 1) {
        head = inode;
        inode->parent = NULL;
      } else {
        inode->parent = toVisit.front();
      }
    } else if (bit == LEAF_NODE_TYPE) {
      lnode = new leaf_node(0); // fill for now with placeholder values
      leaf_nodes.push_back(lnode);
      lnode->parent = toVisit.front();
      if (last_bit) { // add leaf_node to the right of the node in the toVisit list
        toVisit.front()->right = lnode;
        while(!toVisit.empty() && toVisit.front()->value) {
          inode = toVisit.front();
          toVisit.pop_front();
          toVisit.front()->right = inode;
        } 
        if (!toVisit.empty()) {
          toVisit.front()->left = inode;
          toVisit.front()->value = 1; // mark as left side traversed
        }
      } else {
        toVisit.front()->left = lnode;
        toVisit.front()->value = 1; // mark as left side traversed
      }
    }
    last_bit = bit;
    node_count++;
  }
  /* bis.close(); */

  // fill in the missing character values
  for (auto it = leaf_nodes.begin(); it != leaf_nodes.end(); it++) {
    (*it)->c = (char)in.get();
    char_map[(*it)->c] = (*it);
  }
}

// ====== Accessors ======

size_t HuffmanTree::size() {
  return leaf_nodes.size() + intern_nodes.size();
}

HuffmanTree::node* HuffmanTree::get_head() {
  return head;
}

void HuffmanTree::print_tree() {
  // In-order traversal of the tree  
  std::cout << "Printing tree... \n"; 
  std::cout << "* in-order: "; 
  node* n;
  std::list<node*> toVisit = {head};
  while (!toVisit.empty()) {
    n = toVisit.front();
    toVisit.pop_front();
    if (n->type == LEAF_NODE_TYPE) {
      std::cout << " L('" << (char)(((leaf_node*)n)->c) << "'|" << n->value << ")"; 
    } else if (n->type == INTERN_NODE_TYPE) {
      // push children into the toVisit list
      toVisit.push_front(((intern_node*)n)->right);
      toVisit.push_front(((intern_node*)n)->left);
      std::cout << " I(" << n->value << ")"; 
    }
  }
  std::cout << std::endl;

  std::cout << "* leaf_nodes:";
  for (auto it = leaf_nodes.begin(); it != leaf_nodes.end(); it++) {
    std::cout << " '" << (*it)->c << "':" << (*it)->value;
  }
  std::cout << std::endl;

  std::cout << "* intern_nodes:";
  for (auto it = intern_nodes.begin(); it != intern_nodes.end(); it++)
    std::cout << " " << (*it)->value;
  std::cout << std::endl;
}

/*
int main(int argc, char* argv[]) {
  if (argc != 2) { 
    std::cout << "HUFF <file>\n";
    return 0;
  }
  clock_t t0=clock(); // Execution timing

  std::string fstr(argv[1]);
  // get the file input
  std::ifstream ifs(fstr);

  // designate the compressed file output
  int lastindex = fstr.find_last_of(".");
  std::ofstream ofs(fstr.substr(0, lastindex).append(".huf"), std::ofstream::out);
  HuffmanTree ht;
  ht.build(ifs);
  ifs.seekg(std::ios::beg);
  ifs.clear();
  ht.encode(ifs, ofs);

  ifs.close();
  ofs.close();

  std::cout << "Compression took: " << ((float)(clock()-t0))/CLOCKS_PER_SEC << std::endl;

  return 0;
}
//*/

