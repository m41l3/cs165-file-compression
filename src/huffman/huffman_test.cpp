#include <gtest/gtest.h>
#include "huffman.h"
#include <sstream>
#include <string>
#include <fstream>
#include <iostream>
#include <limits.h>

std::string test_str0 = "hello, world!";

TEST(HuffmanTreeTest, TestSanity) {
  HuffmanTree hf;
}

TEST(HuffmanTreeTest, TestBuildFromFile0) {
  std::stringstream ss;
  ss << test_str0;
  HuffmanTree hf;
  hf.build(ss);

  ASSERT_EQ(11 + 10, hf.size());
}

TEST(HuffmanTreeTest, TestBuildFromEnc0) {

  std::stringstream ss;
  ss << test_str0;
  HuffmanTree hf;
  hf.build(ss);

  std::ofstream ofs("test.huf", std::ofstream::out);
  ss.seekg(std::ios::beg);
  ss.clear();
  hf.encode(ss, ofs);
  ofs.close();

  std::ifstream ifs("test.huf");
  HuffmanTree hf1;
  hf1.build(ifs);
  ifs.close();

  ASSERT_EQ(11 + 10, hf1.size());
}

TEST(HuffmanTreeTest, TestEncodeDecode) {
  std::ifstream ifs("test.txt");
  HuffmanTree hf;
  hf.build(ifs);

  std::ofstream ofs("test.huf", std::ofstream::out);
  ifs.seekg(std::ios::beg);
  ifs.clear();
  hf.encode(ifs, ofs);
  ofs.close();

  std::ifstream ifs1("test.huf");
  std::ofstream ofs1("test.out", std::ofstream::out);
  HuffmanTree hf1;
  hf1.build(ifs1);
  hf1.decode(ifs1, ofs1);
  ifs.close();
  ofs.close();

  // TEST
  ifs.seekg(std::ios::beg);
  ifs.clear();
  std::ifstream ifs2("test.out");
  while(ifs.good() || ifs2.good()) {
    ASSERT_EQ(ifs.get(), ifs2.get());
  }
  ifs.close();
  ifs2.close();
}

TEST(HuffmanTreeTest, TestEncodeDecode2) {
  std::ifstream ifs("test2.txt");
  HuffmanTree hf;
  hf.build(ifs);

  std::ofstream ofs("test2.huf", std::ofstream::out);
  ifs.seekg(std::ios::beg);
  ifs.clear();
  hf.encode(ifs, ofs);
  ofs.close();

  std::ifstream ifs1("test2.huf");
  std::ofstream ofs1("test2.out", std::ofstream::out);
  HuffmanTree hf1;
  hf1.build(ifs1);
  hf1.decode(ifs1, ofs1);
  ifs.close();
  ofs.close();

  // TEST
  ifs.seekg(std::ios::beg);
  ifs.clear();
  std::ifstream ifs2("test2.out");
  while(ifs.good() || ifs2.good()) {
    ASSERT_EQ(ifs.get(), ifs2.get());
  }
  ifs.close();
  ifs2.close();
}
